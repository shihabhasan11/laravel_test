#!/bin/bash
echo '[INFO] Copying App Codes to /home/klovercloud/app'
cp -a /klovercloud/tmp/. /home/klovercloud/app
echo '[INFO] Starting Server'
php artisan serve --host=0.0.0.0 --port=8080
